# All Hal

Pour installer les dépendances de **All Hal**, il faut utiliser les commandes suivantes :

_Si vous utilisez uniquement Python :_

```bash
$ pip3 install -r requirements.txt
```

_Si vous utilisez Ananconda :_

```bash
$ conda install --yes --file requirements.txt
```

Une fois les dépendances installées, vous pouvez maintenant utiliser le **Jupyter Notebook** pour exécuter les 2 applications.