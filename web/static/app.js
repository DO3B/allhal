
const SERVER = "http://0.0.0.0:8080/api/search/"
const SECRET = "e8252807b79b82fa0c93d398da0c120bf91b0c0ef436d047ffba34a81b5b9135"

var LogoAllHal = {
  props : {
    logo: {type: String, required: true}
  },
  template: `<div class="container">
                <center>
                  <img v-if="logo == 'hal'" src="logo-hal.png" style="height:200px;" alt="Logo Swag">
                  <img v-if="logo == 'arxiv'" src="logo-arxiv.png" style="height:200px;" alt="Logo Swag">
                </center>
              </div>`,
};

var ResultTable = {
  props : {
    docs: {type: Object, required: true}
  },
  template: `<div v-if="docs.response.start != 1">
                <div class="alert alert-info" v-if="docs.response.numFound > 0">
                  <strong>Wololo !</strong> J'ai trouvé {{docs.response.numFound}} réponses
                </div>
                <div class="alert alert-warning" v-else>
                  <strong>Aie aie aie !</strong> J'ai rien trouvé :'(
                </div>
                <div class="table-responsive">
                  <table class="table" v-if="docs.response.numFound > 0">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Titre</th>
                        <th>Auteur(s)</th>
                        <th>Contributeur(s)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="doc in docs.response.docs">
                        <td>{{doc.docid}}</td>
                        <td>{{doc.title_s[0]}}</td>
                        <td>
                          <span v-for="(auth, index) in doc.authFullName_s">
                            <span v-if="index != doc.authFullName_s.length - 1"> {{auth}},</span>
                            <span v-else> {{auth}}</span>
                          </span>
                        </td>
                        <td>
                          {{doc.contributorFullName_s}}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>`
};

var SearchingForm = {
  template: `<div class="container">
              <logo-all-hal :logo="picked"></logo-all-hal>
              <form v-on:submit.prevent="search" method="post">          
                  <div class="row align-items-center justify-content-center">
                      <div class="col-md-10">
                        <div class="form-group">
                          <label for="name">Recherche :</label>
                          <input type="text" class="form-control" v-model.lazy="research" placeholder="Séparez les différents noms par des ;">
                        </div>
                        <div class="form-group">
                          <label for="search">Type de recherche :</label>
                          <select class="form-control" v-model="selected">
                            <option v-for="option in options" v-bind:value="option.value">
                              {{ option.text }}
                            </option>
                          </select>
                        </div>
                        <center>
                          <div class="form-check form-check-inline" v-for="radio in radios">
                            <input class="form-check-input" type="radio" v-bind:value="radio.value" v-model="picked" v-on:click="update(picked)">
                            <label class="form-check-label">
                              {{ radio.text }}
                            </label>
                          </div>
                        </center>
                        <button v-if="picked == 'hal'" type="submit" class="btn" style="color:#fff;background-color: #108014;border-color: #1c4c1c;">Rechercher</button>
                        <button v-if="picked == 'arxiv'" type="submit" class="btn" style="color:#fff;background-color: #af1818;border-color: #470a0a;">Rechercher</button>
                      </div>
                  </div>
                </form>
                <result-table :docs="docs"></result-table>
              </div>`,
  methods: {
    update: function(picked){
      if(picked == "hal"){
        app.$children[0]._data.options[1].text = "Camarade(s)" 
      } else {
        app.$children[0]._data.options[1].text = "Co-Auteur(s)"
      }
    },
    search: function(){
      axios({
        method: 'get',
        headers: {'x-api-key': SECRET},
        url: SERVER + "" +  this.picked + "&" + this.research + '&' + this.selected
      }).then(function success(response){
        app.$children[0]._data.docs = response.data;
      }, function error(response){
        console.log(response)
      })
    }
  },
  data: function() {
    return {
      docs: {response: {numFound: 0, start: 1}},
      research: null,
      selected: 'author',
      picked: 'hal',
      radios: [
        {text: 'HAL', value: 'hal'},
        {text: 'ArXiV', value: 'arxiv'}
      ],
      options: [
        {text: 'Auteur(s)', value: 'author'},
        {text: 'Co-Auteur(s)', value: 'coauth'},
        {text: 'Autre', value: 'other'}
      ]
    };
  },
  components: {
    'logo-all-hal' : LogoAllHal,
    'result-table': ResultTable
  },
};

var app = new Vue({
  el: '#app',
  components: {
    'searching-form' : SearchingForm
  },
});
